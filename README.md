# dnscrypt-script
Un script qui installe un serveur chiffrant vos DNS

# Instruction d'utilisation :
# A utiliser en superutilisateur

- git clone https://thelongestline@gitlab.com/thelongestline/dnscrypt-script.git

- cd dnscrypt-script

- chmod a+x dnscrypt-script.sh

- bash dnscrypt-script.sh

# Conditions d'utilisation :
- testé sous serveur debian fraîchement installé

- le script part du postulat que votre réseau local est sur l'interface eth0.

- enfin, il vous faut connaitre l'IP locale du serveur sur lequel vous appliquez ce script.

### Merci de vos retour.
